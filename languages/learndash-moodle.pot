# Copyright (C) 2020 Projeto Shift <geral@meuppt.pt>
# This file is distributed under the same license as the Learndash Moodle Integration package.
msgid ""
msgstr ""
"Project-Id-Version: Learndash Moodle Integration 0.1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/learndash-moodle\n"
"POT-Creation-Date: 2020-08-28 18:50:54+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 0.5.4\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#. Plugin Name of the plugin/theme
msgid "Learndash Moodle Integration"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "PLUGIN SITE HERE"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Offers a variety of tools to exchange data, migrate contents, integrate and "
"operate Moodle and Learndash instances at the same time."
msgstr ""

#. Author of the plugin/theme
msgid "Projeto Shift <geral@meuppt.pt>"
msgstr ""

#. Author URI of the plugin/theme
msgid "YOUR SITE HERE"
msgstr ""