<?php

namespace MoodleLD\Api;

interface Context {

	function newCall($method, $payload);

	function testApiAvailability();

}
