<?php

namespace MoodleLD\Api;

interface Call {

	function __construct(
			$url,
			$method,
			Array $payload);

	function execute();

}
