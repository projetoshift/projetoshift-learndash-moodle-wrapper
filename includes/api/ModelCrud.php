<?php

namespace MoodleLD\Api;
use MoodleLD\Api\Context;

interface ModelCrud {

	function get(Context $apiContext);

	function create(Context $apiContext);

	function update(Context $apiContext);

	function delete(Context $apiContext);

}
