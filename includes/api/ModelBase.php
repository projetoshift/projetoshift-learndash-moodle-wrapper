<?php

namespace MoodleLD\Api;
use MoodleLD\Api\Context;
use MoodleLD\Utils\Reflection;

abstract class ModelBase {

	public function __construct() {}

	public function apiCall(
			Context $apiContext,
			$method = '',
			$payload = null) {
	
		$call = $apiContext->newCall(
			$method,
			$payload);

		$response = $call->execute();

		return $response;
	}
}
