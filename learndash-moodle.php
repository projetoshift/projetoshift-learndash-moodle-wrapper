<?php
/**
 * Plugin Name:     Learndash Moodle Integration
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     Offers a variety of tools to exchange data, migrate contents, integrate and operate Moodle and Learndash instances at the same time.
 * Author:          Projeto Shift <geral@meuppt.pt>
 * Author URI:      YOUR SITE HERE
 * Text Domain:     learndash-moodle
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Learndash_Moodle
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version and constants.
 * Starts at 1.0.0
 */
define( 'LD_MOODLE_VERSION', '1.0.0' );
define( 'LD_MOODLE_PATH', plugins_url() . '/learndash-moodle/' );

/**
 * Autoload and class management with Composer
 * @since    1.0.0
 */
require __DIR__ . '/vendor/autoload.php';
